import math

try:
    radius = float(input("Enter the radius"))
except ValueError:
    print("Please enter a number")
else:
    print("radius is {}".format(math.pi*(radius**2)))