string = input("Enter a string: ")

#The islower() method returns True if all alphabets in a string are lowercase
#alphabets. If the string contains at least one uppercase alphabet, it returns False.

print(string.islower())

#The isupper() method returns True if all alphabets in a string are uppercase
#alphabets. If the string contains at least one lowercase alphabet, it returns False.

print(string.isupper())

#The isspace() method returns True if there are only whitespace characters in the
#string. If not, it return False.

print(string.isspace())

#The string lower() method converts all uppercase characters in a string into
#lowercase characters and returns it.

print(string.lower())

#The string upper() method converts all lowercase characters in a string into
#uppercase characters and returns it.

print(string.upper())