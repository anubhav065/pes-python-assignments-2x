import math

den = int(input("Enter the denominator"))
#rad2 = int(input("Enter the second value"))

print("sine of pi/den")
print(math.sin(math.pi/den))

print("cosine of pi/den")
print(math.cos(math.pi/den))

print("cotangent of pi/den")
print(math.tan(math.pi/den))

print("arc sine of pi/den")
print(math.asin(math.pi/den))

print("arc cosine of pi/den")
print(math.acos(math.pi/den))

print("arc cotangent of pi/den")
print(math.atan(math.pi/den))
