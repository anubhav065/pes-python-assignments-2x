string = input("Enter a string: ")

#The string format() method formats the given string into a nicer output in Python.
print(string+" {}".format('string added using format'))

#The index() method returns the index of a substring inside the string (if found).
#If the substring is not found, it raises an exception.
try:
    print(string.index('test'))
except ValueError:
    print("Sorry! Exxeption caught!, Please enter a string next time!")
    
#The isalnum() method returns True if all characters in the string are alphanumeric
#(either alphabets or numbers). If not, it returns False.
print(string.isalnum())

#The isalpha() method returns True if all characters in the string are alphabets.
#If not, it returns False.
print(string.isalpha())

#The isdecimal() method returns True if all characters in a string are decimal
#characters. If not, it returns False.
print(string.isdecimal())

