# \newline
print("line1 \
line2 \
line3")

print("#####################")

# \\
print("\\")

print("#####################")

# \'
print("\'")

print("#####################")

# \"
print("\"")

print("#####################")

# \b
print("hello\b\b\b\b world")

print("#####################")

# \n
print("hello \nworld")

print("#####################")

# \t
print("hello\tworld")

print("#####################")

# \v
print("hello\vworld")

print("#####################")

# \ooo
print("\110\145\154\154\157\40\127\157\162\154\144\41")

print("#####################")

# \xhh
print("\x48\x65\x6c\x6c\x6f\x20\x57\x6f\x72\x6c\x64\x21")
