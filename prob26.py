print('encoded string: edvlf fubswrjudskb')

encoded = 'edvlffubswrjudskb'

print('Algorithm: shift each character to left by 3 positions')

list_ = []

for i in encoded:
    if ord(i)-3 < 97:
        list_.append(chr(26 + ord(i) - 3))
    else:
        list_.append(chr(ord(i) - 3))
        
decoded = "".join(list_)
print(decoded)